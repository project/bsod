<?php

/**
 * @file
 * Implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in page.tpl.php.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 * @see bartik_process_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $bsod_css; ?>
  <?php print $bsod_scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <span class="head"><?php print $site_name; ?></span>
  <p>The website encountered an unexpected error.</p>
  <p>Press CTRL+ALT+DEL to restart your computer. If you do this,<br/>
   you will lose any unsaved information in all open applications.</p>
  <p>Error: </p>
  <p><?php print $messages; ?></p>
  <p>Press any key to continue <blink>_</blink></p>
</body>
</html>
